Desktop version only

# Learning

- Use React components.
- Use "useState".
- Lift up state.
- Use props.
- Use "useEffect".

# How to use 

- Type the movie name into the search bar.
- Select a movie from the list.
- Evaluate the movie using stars and add it to the list.
- The program will calculate how many movies you have watched, the average IMDb rating, your average rating, and the average movie length.
